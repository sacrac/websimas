from eventos.models import Evento
from otras.models import MenuFoto

def globales(request):
	eventos = Evento.objects.order_by('-fecha')[:2]
	foto_publicacion = MenuFoto.objects.get(menu=2)

	return {'latest_events': eventos, 'foto_publicacion': foto_publicacion} 