# -*- coding: utf-8 -*-
from django.db import models
from websimas.utils import get_file_path
from sorl.thumbnail import ImageField

# Create your models here.

class FotoPortada(models.Model):
    texto1 = models.CharField(max_length=250)
    texto2 = models.CharField(max_length=250)
    foto = ImageField(upload_to=get_file_path)

    fileDir = 'portada/'

    def __unicode__(self):
        return self.texto1
