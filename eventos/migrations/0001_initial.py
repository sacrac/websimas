# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields
import websimas.utils


class Migration(migrations.Migration):

    dependencies = [
        ('publicaciones', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Evento',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('evento', models.CharField(max_length=400)),
                ('objetivo', models.TextField(null=True, blank=True)),
                ('fecha', models.DateField(null=True, blank=True)),
                ('lugar', models.CharField(max_length=400, null=True, blank=True)),
                ('contacto', models.CharField(max_length=200, null=True, blank=True)),
                ('direccion', models.CharField(max_length=200, null=True, blank=True)),
                ('telefono', models.CharField(max_length=35, null=True, blank=True)),
                ('fax', models.CharField(max_length=35, null=True, blank=True)),
                ('apartado', models.CharField(max_length=25, null=True, blank=True)),
                ('correo', models.CharField(max_length=150, null=True, blank=True)),
                ('web', models.CharField(max_length=160, null=True, blank=True)),
                ('fechaingreso', models.DateField(null=True, blank=True)),
                ('archivo', models.CharField(max_length=500, null=True, blank=True)),
                ('logotipo', sorl.thumbnail.fields.ImageField(upload_to=websimas.utils.get_file_path, null=True, verbose_name=b'logo Evento', blank=True)),
                ('estado', models.CharField(max_length=1, null=True, blank=True)),
                ('organiza', models.CharField(max_length=200, null=True, blank=True)),
                ('uri', models.CharField(max_length=500, null=True, blank=True)),
                ('ciudad', models.CharField(max_length=200, null=True, blank=True)),
                ('idtematica', models.ManyToManyField(to='publicaciones.Tematica', null=True, verbose_name=b'Tematica', blank=True)),
            ],
            options={
                'verbose_name_plural': 'Eventos',
            },
            bases=(models.Model,),
        ),
    ]
