# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields
import ckeditor_uploader.fields
import websimas.utils


class Migration(migrations.Migration):

    dependencies = [
        ('publicaciones', '__first__'),
        ('otras', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Noticia',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('noticia', models.CharField(max_length=350, null=True, blank=True)),
                ('fecha', models.DateField(null=True, blank=True)),
                ('importante', models.NullBooleanField(verbose_name=b'En portada')),
                ('fuente', models.CharField(max_length=100, null=True, blank=True)),
                ('estado', models.CharField(max_length=1, null=True, blank=True)),
                ('texto', ckeditor_uploader.fields.RichTextUploadingField(null=True, blank=True)),
                ('claves', models.CharField(max_length=255, null=True, blank=True)),
                ('descripcion', models.TextField(null=True, blank=True)),
                ('uri', models.CharField(max_length=500, null=True, blank=True)),
                ('foto', sorl.thumbnail.fields.ImageField(upload_to=websimas.utils.get_file_path, null=True, verbose_name=b'Foto principal', blank=True)),
                ('url', models.URLField(null=True, blank=True)),
                ('tipo', models.IntegerField(blank=True, null=True, choices=[(1, b'Texto'), (2, b'Video')])),
                ('credito', models.CharField(max_length=200, null=True, blank=True)),
                ('resumen', models.TextField(null=True, blank=True)),
                ('titarchivo1', models.CharField(max_length=200, null=True, verbose_name=b'Titulo 1', blank=True)),
                ('titarchivo2', models.CharField(max_length=200, null=True, verbose_name=b'Titulo 2', blank=True)),
                ('archivo', models.CharField(max_length=500, null=True, verbose_name=b'Titulo 3', blank=True)),
                ('archivo1', models.FileField(null=True, upload_to=websimas.utils.get_file_path, blank=True)),
                ('archivo2', models.FileField(null=True, upload_to=websimas.utils.get_file_path, blank=True)),
                ('archivo3', models.FileField(null=True, upload_to=websimas.utils.get_file_path, blank=True)),
                ('idautor', models.ForeignKey(verbose_name='Publicado por: ', blank=True, to='otras.Autor', null=True)),
                ('idtematica', models.ManyToManyField(to='publicaciones.Tematica', null=True, verbose_name='Tematicas', blank=True)),
            ],
            options={
                'verbose_name_plural': 'Noticias',
            },
            bases=(models.Model,),
        ),
    ]
