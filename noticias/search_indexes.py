import datetime
from haystack import indexes
from .models import Noticia


class NoticiaIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    noticia = indexes.CharField(model_attr='noticia')
    resumen = indexes.CharField(model_attr='resumen', null=True)
    claves = indexes.CharField(model_attr='claves', null=True)
    idautor = indexes.CharField(model_attr='idautor', null=True)
    #fecha = indexes.DateField(model_attr='fecha', null=True)

    content_auto = indexes.EdgeNgramField(model_attr='noticia')

    def get_model(self):
        return Noticia

    def index_queryset(self, using=None):
        return self.get_model().objects.all()