# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields
import ckeditor.fields
import websimas.utils


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Aliado',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('correo', models.CharField(max_length=255, null=True, blank=True)),
                ('direccion', models.CharField(max_length=255, null=True, blank=True)),
                ('descripcion', ckeditor.fields.RichTextField(null=True, blank=True)),
                ('logo', sorl.thumbnail.fields.ImageField(upload_to=websimas.utils.get_file_path, null=True, verbose_name=b'Logo oficial', blank=True)),
                ('aliado', models.CharField(max_length=255, null=True, blank=True)),
                ('uri', models.CharField(max_length=500, null=True, editable=False, blank=True)),
                ('web', models.CharField(max_length=300, null=True, blank=True)),
            ],
            options={
                'verbose_name': 'Aliado',
                'verbose_name_plural': 'Aliados',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Autor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('autor', models.CharField(max_length=500, null=True, blank=True)),
                ('email', models.CharField(max_length=150, null=True, blank=True)),
                ('imagen', models.CharField(max_length=500, null=True, blank=True)),
            ],
            options={
                'verbose_name': 'Autor',
                'verbose_name_plural': 'Autores',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Financiadores',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('logo', models.CharField(max_length=500, null=True, blank=True)),
                ('nombre', models.CharField(max_length=120, null=True, blank=True)),
                ('web', models.CharField(max_length=500, null=True, blank=True)),
                ('orden', models.IntegerField(null=True, blank=True)),
            ],
            options={
                'verbose_name': 'Financiador',
                'verbose_name_plural': 'Financiadores',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MenuFoto',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('menu', models.IntegerField(choices=[(1, b'quienes-somos'), (2, b'publicaciones'), (3, b'portafolio'), (4, b'noticias'), (5, b'blogs'), (6, b'contactenos'), (7, b'historia')])),
                ('titulo', models.CharField(max_length=250)),
                ('foto', sorl.thumbnail.fields.ImageField(upload_to=websimas.utils.get_file_path)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Pais',
            fields=[
                ('idpais', models.CharField(max_length=2, serialize=False, primary_key=True)),
                ('pais', models.CharField(max_length=80, null=True, blank=True)),
                ('region', models.CharField(max_length=2, null=True, blank=True)),
                ('telefono', models.CharField(max_length=3, null=True, blank=True)),
            ],
            options={
                'verbose_name': 'Pais',
                'verbose_name_plural': 'Paises',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Servicio',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('servicio', models.CharField(max_length=255, null=True, blank=True)),
                ('descripcion', ckeditor.fields.RichTextField(null=True, blank=True)),
                ('imagen', sorl.thumbnail.fields.ImageField(upload_to=websimas.utils.get_file_path, null=True, verbose_name=b'Foto', blank=True)),
                ('uri', models.CharField(max_length=500, null=True, blank=True)),
            ],
            options={
                'verbose_name': 'Servicio',
                'verbose_name_plural': 'Servicios',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Sistema',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sistema', models.CharField(max_length=255, null=True, blank=True)),
                ('foto', sorl.thumbnail.fields.ImageField(upload_to=websimas.utils.get_file_path, null=True, verbose_name=b'Foto sistema', blank=True)),
                ('url', models.CharField(max_length=255, null=True, blank=True)),
                ('fecha', models.DateField(null=True, blank=True)),
            ],
            options={
                'verbose_name': 'Sistema',
                'verbose_name_plural': 'Sistemas',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Trabajadores',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombres', models.CharField(max_length=250, verbose_name=b'Nombres y apellidos')),
                ('foto', sorl.thumbnail.fields.ImageField(null=True, upload_to=websimas.utils.get_file_path, blank=True)),
                ('correo', models.EmailField(max_length=75)),
                ('cargo', models.CharField(max_length=50, verbose_name=b'cargo')),
            ],
            options={
                'verbose_name_plural': 'Trabajadores',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UbicacionTrabajor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ubicacion', models.CharField(max_length=50)),
            ],
            options={
                'verbose_name_plural': 'Ubicaci\xf3n del trabajador',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='trabajadores',
            name='ubicacion',
            field=models.ForeignKey(to='otras.UbicacionTrabajor'),
            preserve_default=True,
        ),
    ]
