from django.contrib import admin
from .models import Aliado, Servicio, Sistema, Financiadores, Autor, Pais, UbicacionTrabajor, Trabajadores, MenuFoto

# Register your models here.
admin.site.register(Aliado)
admin.site.register(Servicio)
admin.site.register(Sistema)
admin.site.register(Financiadores)
admin.site.register(Autor)
admin.site.register(Pais)
admin.site.register(UbicacionTrabajor)
admin.site.register(Trabajadores)
admin.site.register(MenuFoto)