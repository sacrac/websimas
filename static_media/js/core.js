//we better be safe :)
//jQuery.noConflict();

$(function() {
    $(window).scroll(function() {
        var x = $("header[role=main]");
        var xheight = $("header[role=main]").height();
        if ( $(this).scrollTop() >= xheight ) {
            x.addClass('fixed');
        } else {
            x.removeClass('fixed');
        }
    });
});

// jQuery(document).ready(function(){
//     var hdr = document.querySelector("header[role=main]");
//     var headroom  = new Headroom(hdr);
//     headroom.init();
// });