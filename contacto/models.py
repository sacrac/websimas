from django.db import models
from ckeditor.fields import RichTextField

# Create your models here.

class Contacto(models.Model):
	texto = RichTextField()

	def __unicode__(self):
		return "Contacto"
		