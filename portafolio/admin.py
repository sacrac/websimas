# -*- coding: utf-8 -*-
from django.contrib import admin
from .models import *
from fotos.models import Fotos

class FotosAdmin(generic.GenericTabularInline):
    model = Fotos
    extra = 1

class PortafolioAdmin(admin.ModelAdmin):
    inlines = [FotosAdmin]

    list_display = ('servicio', 'fecha', 'link')


# Register your models here.
admin.site.register(Organizaciones)
admin.site.register(Portafolio, PortafolioAdmin)