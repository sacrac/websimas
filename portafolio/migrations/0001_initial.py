# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Organizaciones',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=250)),
                ('iniciales', models.CharField(max_length=150)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Portafolio',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('servicio', models.CharField(max_length=250, verbose_name=b'Nombre del servicio')),
                ('resumen', models.TextField()),
                ('fecha', models.DateField(verbose_name=b'Fecha de realizaci\xc3\xb3n')),
                ('link', models.URLField(blank=True)),
                ('organizacion', models.ManyToManyField(to='portafolio.Organizaciones')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
