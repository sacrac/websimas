# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('publicaciones', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Audio',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('audio', models.CharField(max_length=255, blank=True)),
                ('embed', models.TextField(blank=True)),
                ('archivomp3', models.CharField(max_length=500, blank=True)),
                ('fecha_ini', models.DateField(null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Cataudio',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cataudio', models.CharField(max_length=250, blank=True)),
                ('ordencataudio', models.IntegerField(null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Estadovideo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('estadovideo', models.CharField(max_length=30, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Video',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('video', models.CharField(max_length=255, null=True, blank=True)),
                ('texto', models.TextField(null=True, blank=True)),
                ('url', models.CharField(max_length=300, null=True, blank=True)),
                ('claves', models.CharField(max_length=255, null=True, blank=True)),
                ('creacion', models.DateTimeField(null=True, blank=True)),
                ('archivo', models.CharField(max_length=500, null=True, blank=True)),
                ('idusuario', models.IntegerField(null=True, blank=True)),
                ('preview', models.CharField(max_length=500, null=True, blank=True)),
                ('youtube', models.CharField(max_length=500, null=True, blank=True)),
                ('idtematica', models.IntegerField(null=True, blank=True)),
                ('cidoc', models.NullBooleanField()),
                ('codigo', models.CharField(max_length=50, null=True, blank=True)),
                ('duracion', models.CharField(max_length=50, null=True, blank=True)),
                ('anho', models.CharField(max_length=4, null=True, verbose_name=b'A\xc3\xb1o', blank=True)),
                ('realizacion', models.CharField(max_length=255, null=True, blank=True)),
                ('pais', models.CharField(max_length=50, null=True, blank=True)),
                ('googleid', models.CharField(max_length=500, null=True, blank=True)),
                ('fecha', models.DateField(null=True, blank=True)),
                ('titulo2', models.CharField(max_length=300, null=True, blank=True)),
                ('estado', models.ForeignKey(verbose_name=b'Estado del video', blank=True, to='multimedia.Estadovideo', null=True)),
                ('tematica', models.ForeignKey(blank=True, to='publicaciones.Tematica', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='audio',
            name='idcataudio',
            field=models.ForeignKey(blank=True, to='multimedia.Cataudio', null=True),
            preserve_default=True,
        ),
    ]
